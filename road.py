# Settlers 2 inspired road simulator.
# Ben Kelly <ben@benjii.net>

import curses
import random
import logging
import datetime
import time

from priodict import priorityDictionary

logging.basicConfig(filename='road.log', level=logging.DEBUG)

# Chance a road will not be formed between nodes
ROAD_CHANCE = 0.0

# Give each road a length. This should be in "ticks"
ROAD_LENGTH = 20

# Tick rate in ticks per second. 0 = no limit.
TICK_RATE = 60

# Max number of item slots at a node.
NODE_ITEM_SLOTS = 8

# Apply a cost to a road after this many slots have been consumed
SLOT_COST_THRESHOLD = 6

# The amount added to the pathing cost for each item at a neighboring node.
SLOT_COST = 8


class Node:
    """ Represents a point connected by one or more roads. """

    def __init__(self, x, y):
        self.x = x
        self.y = y

        # List of neighboring nodes
        self.neighbors = []

        # List of items stored at this node
        self.items = []

        # A counter of item slots. slots are consumed by local items and spares
        # can be assigned to incoming carriers
        self.item_slots = 0

        self.carriers = []

    def __repr__(self):
        return "<Node at [{0},{1}] {2}N>".format(self.x, self.y, len(self.neighbors))

    def add_neighbor(self, n):
        if n not in self.neighbors: self.neighbors.append(n)

    def remove_neighbor(self, n):
        self.neighbors = [x for x in self.neighbors if x != n]

    def has_neighbor(self, n):
        if n in self.neighbors:
            return True
        else:
            return False

    def add_item(self, i):
        if i not in self.items:
            self.items.append(i)
            i.reset_node_ticker()
            i.node = self

    def remove_item(self, i):
        self.items = [x for x in self.items if x != i]
        self.item_slots -= 1

    def item_count(self):
        return len(self.items)

    def slot_count(self):
        return self.item_slots

    def request_item_slot(self):
        if self.item_slots < NODE_ITEM_SLOTS:
            self.item_slots += 1
            return True
        else:
            return False

    def reset_slots(self):
        self.item_slots = len(self.items)


class Map:
    """ The map of nodes and roads """

    # The map dimensions
    width = 0
    height = 0

    # Dictionary of nodes in the map
    nodes = {}

    # List of roads. Each item in the list should be a 2-tuple containing start
    # node and end node. Nodes must be adjacent vertically or horz.
    roads = []

    # List of carriers
    carriers = []

    def __init__(self, width, height):
        self.width = width
        self.height = height

        self.fill_nodes()

    def fill_nodes(self):
        for x in range(self.width):
            for y in range(self.height):
                self.nodes[(x,y)] = Node(x, y)

    def create_roads(self):
        for coord, n in self.nodes.items():
            self.add_road(coord, n, 1, 0)
            self.add_road(coord, n, -1, 0)
            self.add_road(coord, n, 0, 1)
            self.add_road(coord, n, 0, -1)

    def add_road(self, coord, n, dx, dy):
        n_x, n_y = coord
        if random.random() > ROAD_CHANCE:
            neighbor_coord = (n_x + dx, n_y + dy)

            if self.nodes.has_key(neighbor_coord):
                n2 = self.nodes[neighbor_coord]
                if not n.has_neighbor(n2): n.add_neighbor(n2)
                if not n2.has_neighbor(n): n2.add_neighbor(n)

                if (n,n2) not in self.roads and (n2,n) not in self.roads:
                    self.roads.append((n,n2))
                    c = Carrier(n,n2)
                    self.carriers.append(c)
                    n.carriers.append(c)
                    n2.carriers.append(c)

    def find_carrier(self, n1, n2):
        for c in self.carriers:
            if ((c.node_1 == n1 and c.node_2 == n2) or
                (c.node_1 == n2 and c.node_2 == n1)):
                return c
        return False


class Carrier:
    """ Carriers occupy a road and carry items between nodes. """

    def __init__(self, n_1, n_2):
        self.node_1 = n_1 # position 0
        self.node_2 = n_2 # position ROAD_LENGTH

        # position determines the Carries spot along the road. 0 being at
        # node_1, ROAD_LENGTH being at node_2.
        self.position = ROAD_LENGTH / 2

        # The item that the carrier is/will be carrying
        self.item = None
        self.item_node = None
        self.item_dest = None

        # Flags that track the state of the carrying process.
        self.state_picking_up     = False
        self.state_dropping_off   = False
        self.state_moving_to_mid  = False
        self.state_waiting_at_mid = False

        # Flag that tracks whether this carrier is currently doing nothing and
        # avaliable to carry an item
        self.available = True

    def has_item(self):
        if self.item and (self.state_moving_to_mid or
                          self.state_dropping_off or
                          self.state_waiting_at_mid):
            return True
        else:
            return False

    def pick_up(self, item):
        self.available = False
        self.state_picking_up = True
        self.item = item
        self.item_node = item.node
        if self.item_node == self.node_1:
            self.item_dest = self.node_2
        else:
            self.item_dest = self.node_1
        item.carrier = self
        logging.debug('carrier pick up')

    def move(self):
        if self.state_picking_up: 
            logging.debug('carrier moving {0}'.format(self.position))
            # Move towards item_node
            if self.item_node == self.node_1:
                self.position -= 1
            else:
                self.position += 1
        if self.state_dropping_off:
            logging.debug('carrier moving {0}'.format(self.position))
            # Move away from item_node
            if self.item_node == self.node_1:
                self.position += 1
            else:
                self.position -= 1
        if self.state_moving_to_mid:
            logging.debug('carrier moving {0}'.format(self.position))
            if self.position > ROAD_LENGTH / 2:
                self.position -= 1
            else:
                self.position += 1

        # Check if we reached destination:
        if self.state_moving_to_mid and self.position == ROAD_LENGTH / 2:
            self.reached_mid()

        if self.state_picking_up and (self.position == 0 or
                                      self.position == ROAD_LENGTH):
            self.pick_up_item()

        if self.state_dropping_off and (self.position == 0 or
                                        self.position == ROAD_LENGTH):
            self.drop_off_item()

    def reached_mid(self):
        self.state_moving_to_mid = False

        if self.item is None:
            # We've just finished dropping off - reset state of carrier.
            self.item = None
            self.item_node = None
            self.item_dest = None
            self.available = True
        else:
            self.state_waiting_at_mid = True

    def pick_up_item(self):
        # We have arrived at item_node, need to pick up the item and head back.
        self.state_picking_up = False
        self.state_moving_to_mid = True

        self.item.node.remove_item(self.item)
        self.item.node = None

    def drop_off_item(self):
        # We have arrived at item's destination. drop off item.
        self.state_dropping_off = False
        self.state_moving_to_mid = True

        self.item_dest.add_item(self.item)
        self.item.carrier = None
        self.item = None

    def check_dest_if_waiting(self):
        if self.state_waiting_at_mid == True:
            if self.item_dest.request_item_slot():
                self.state_waiting_at_mid = False
                self.state_dropping_off = True

class Display:
    """ Displays the map and roads etc. """

    def __init__(self, screen, rmap):
        self.screen = screen
        self.rmap = rmap

    def node_to_screen(self, x, y):
        # Space out x by 8 characters - one space between road dots so that
        # things look square-ish
        # Space out y by 4 characters
        return (x * 8 + 1, y * 4 + 1)

    def get_road_point(self, n_1, n_2, point):
        # Returns a 2-tuple containin the x,y coordinate of a point in a road.
        # Can be either point 1, 2 or 3.
        cur_x, cur_y = self.node_to_screen(n_1.x, n_1.y)
        x = cur_x + (n_2.x - n_1.x) * (point * 2)
        y = cur_y + (n_2.y - n_1.y) * point
        return (x,y)

    def render_full(self):
        self.render_all_nodes()
        self.render_all_roads()
        self.render_all_carriers()

        #for i in range(-1, 8):
        #    self.screen.addstr(0, i+2, str(i+2), curses.color_pair(i+2))

    def render_all_nodes(self):
        for y in range(self.rmap.height):
            for x in range(self.rmap.width):
                cur_x, cur_y = self.node_to_screen(x, y)
                items = self.rmap.nodes[(x,y)].item_count()
                if items <= 2:
                    color = 4 # Green
                elif items <= 6:
                    color = 5 # Yellow
                else:
                    color = 3 # Red
                self.screen.addstr(cur_y, cur_x, str(items),
                                   curses.color_pair(color))

    def render_all_roads(self):
        for road in self.rmap.roads:
            start_n, end_n = road

            for i in range(1,4):
                x, y = self.get_road_point(start_n, end_n, i)
                self.screen.addstr(y, x, "+", curses.color_pair(2))

    def render_all_carriers(self):
        for carrier in self.rmap.carriers:
            # Determine the screen position of the carrier
            if carrier.position < ROAD_LENGTH / 3:
                pos = 1
            elif carrier.position < ROAD_LENGTH - (ROAD_LENGTH / 3):
                pos = 2
            else:
                pos = 3

            x, y = self.get_road_point(carrier.node_1, carrier.node_2, pos)

            attr = curses.color_pair(1)
            if carrier.has_item():
                attr = curses.color_pair(8) | curses.A_BOLD

            self.screen.addstr(y, x, "o", attr)


class Item:
    """ An item """

    items = []

    def __init__(self, source, dest):
        # The node this item is occupying
        self.node = source

        # Item's destination node
        self.destination = dest

        self.carrier = None
        self.node_ticker = 0
        self.items.append(self)

        # Store the value of the next hop and the tick value it was generated
        # This is to stop multiple path calculations in the same tick.
        self.next_hop = None
        self.next_hop_tick = 0

    def __repr__(self):
        return "<Item at {0},{1} T{2}>".format(self.node.x,
                                               self.node.y,
                                               self.node_ticker)

    def reset_node_ticker(self):
        self.node_ticker = 0

    def delete(self):
        if self.node is not None:
            self.node.remove_item(self)
        #TODO : probably need to handle removing from a carrier at some point
        self.items = [i for i in self.items if i != self]


def do_traffic(rmap, tick):
    # Loop through all carriers looking for ones that are available
    for carrier in rmap.carriers:
        if carrier.available and (carrier.node_1.item_count() or
                                  carrier.node_2.item_count()):
            # We found an idle carrier and waiting items. Check for items
            # that can be carried by this carrier.
            sorted_items = carrier.node_1.items[:]
            for item in carrier.node_2.items:
                found_spot = False
                for i, s_item in enumerate(sorted_items):
                    if item.node_ticker >= s_item.node_ticker and not found_spot:
                        sorted_items.insert(i, item)
                        found_spot = True
                        break

                if not found_spot:
                    sorted_items.append(item)

            # Filter out items that are already being picked up by a carrier
            sorted_items = [i for i in sorted_items if i.carrier is None and i.node != i.destination]

            # Try to find an item who's best path uses this carrier
            for item in sorted_items:
                if item.next_hop_tick == tick and item.next_hop:
                    next_node = item.next_hop
                else:
                    next_node = next_hop(rmap, item.node, item.destination)
                item.next_hop = next_node
                item.next_hop_tick = tick
                if next_node == carrier.node_1 or next_node == carrier.node_2:
                    carrier.pick_up(item)
                    break

def road_dijkstra(rmap, start_n, end_n):
    D = {} # Final distances
    P = {} # Predecessors

    Q = priorityDictionary() # est. dist. of non-final node
    Q[start_n] = 0

    for node in Q:
        D[node] = Q[node]
        if node == end_n: break

        for neighbor in node.neighbors:
            # Calculate an extra cost of the consumed slots is over the slot threshold
            s_cost = 0
            if neighbor.slot_count() > SLOT_COST_THRESHOLD:
                s_cost = (neighbor.slot_count() - SLOT_COST_THRESHOLD) * SLOT_COST
            # Check if the carrier between these nodes is busy and apply a cost if so
            carrier_cost = 0
            carrier = rmap.find_carrier(node, neighbor)
            if not carrier.available:
                carrier_cost = ROAD_LENGTH
            cost = D[node] + ROAD_LENGTH + s_cost + carrier_cost
            if neighbor in D:
                if cost < D[neighbor]:
                    #raise ValueError, "road_dijkstra: found better path to already-final node"
                    pass
            elif neighbor not in Q or cost < Q[neighbor]:
                Q[neighbor] = cost
                P[neighbor] = node
    return (D, P)

def shortest_path(rmap, start_n, end_n):
    D, P = road_dijkstra(rmap, start_n, end_n)
    path = []
    while 1:
        path.append(end_n)
        if end_n == start_n: break
        end_n = P[end_n]
    path.reverse()
    return path

def next_hop(rmap, start_n, end_n):
    path = shortest_path(rmap, start_n, end_n)
    return path[1]

def update_item_tickers(rmap):
    # Update tickers of all items that are currently sitting on a node
    for node in rmap.nodes.itervalues():
        for item in node.items:
            item.node_ticker += 1

def main(screen):
    curses.start_color()
    curses.use_default_colors()

    for i in range(-1,8):
        curses.init_pair(i+2, i, -1)

    rmap = Map(12,8)
    rmap.create_roads()

    # Add some dummy data
    dst = rmap.nodes[(rmap.width - 1, rmap.height - 1)]

    src = rmap.nodes[(0,0)]
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 89
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 72
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 53
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 42
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 13
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 8
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 4
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 1
    src.reset_slots()

    src = rmap.nodes[(7,1)]
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 134
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 45
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 34
    src.add_item(Item(src,dst))
    src.items[src.item_count() - 1].node_ticker = 7
    src.reset_slots()

    src = rmap.nodes[(3,1)]
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.reset_slots()

    src = rmap.nodes[(0,5)]
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.add_item(Item(src,dst))
    src.reset_slots()

    disp = Display(screen, rmap)
    disp.render_full()

    ticks = 0
    if TICK_RATE != 0:
        tick_utime = int((1.0 / TICK_RATE) * 1000000)

    while 1:
        start_time = datetime.datetime.now()

        #c = screen.getch()
        #if c == ord('q'): break
        ticks += 1

        do_traffic(rmap, ticks)

        for carrier in rmap.carriers:
            if not carrier.available:
                carrier.move()
                carrier.check_dest_if_waiting()

        update_item_tickers(rmap)

        disp.render_full()
        screen.refresh()

        logging.debug('')
        logging.debug(' TICK')
        logging.debug('')

        if TICK_RATE != 0:
            end_time = datetime.datetime.now()
            run_utime = tick_utime - (end_time - start_time).microseconds
            sleep_time = run_utime / 1000000.0
            if sleep_time >= 0 and sleep_time <= tick_utime:
                time.sleep(sleep_time)


if __name__ == "__main__":
    curses.wrapper(main)
